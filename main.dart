import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, // removes the debug banner
      theme: ThemeData(primarySwatch: Colors.blue),
      home: const SplashScreen(),
    );
  }
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: Column(children: [
        Text(
          "24DELIVER",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
        ),
        Text('Get your delivery today', style: TextStyle(fontSize: 20))
      ]),
      nextScreen: const Login(),
      duration: 3500,
    );
  }
}

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(elevation: 0, title: const Center(child: Text("Login"))),
      body: SingleChildScrollView(
        child: Column(children: const [
          Padding(
            padding: EdgeInsets.only(right: 85, left: 85, top: 15, bottom: 0),
            child: TextField(
                decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Email",
              hintText: "Enter your Email",
            )),
          ),
          Padding(
            padding: EdgeInsets.only(right: 85, left: 85, top: 15, bottom: 0),
            child: TextField(
                decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Password",
              hintText: "Enter your Password",
            )),
          ),
          ElevatedButton(onPressed: null, child: Text("Login"))
        ]),
      ),
    );
  }
}
